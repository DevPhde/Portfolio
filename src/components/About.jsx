import aboutBackground from "../assets/narutoDark.jpg"
import TypeWriter from "./TypeWriter"
import "../styles/About.css"

function About() {
    const message = [
        "Olá, bem-vindo ao meu portfólio",
        "Meu nome é Diego e eu sou desenvolvedor FullStack."
    ]

    return (
        <div className='about section' id="about">
            <img src={aboutBackground} className="image" alt="naruto" />
            <div className="myMessage">
                <h3 className="w-75"><TypeWriter messages={message} delay={100} /></h3>
            </div>
            <div className="about_button">
                <button className="btn button_quality px-5"><b>Clique Aqui!</b></button>
            </div>
        </div>
    )
}

export default About


