import Container from 'react-bootstrap/Container';
import Nav from 'react-bootstrap/Nav';
import Navbar from 'react-bootstrap/Navbar';
import { Link } from "react-scroll"
import "../styles/Header.css"

function Header() {
  return (
    <header className='pb-5'>
      <Navbar fixed="top" bg="dark" expand="lg">
        <Container fluid>
          <Navbar.Brand className="text-white p-2 header_title">Diego Baumbach</Navbar.Brand>
          <Navbar.Toggle aria-controls="navbarScroll" className='bg-white' />
          <Navbar.Collapse id="navbarScroll">
            <Nav
              className="ms-auto my-2 my-lg-0"
              style={{ maxHeight: '100px' }}
              navbarScroll
            ><li className='d-flex'>
                <ul className='nav-item'>
                  <Link activeClass="active" to="about" spy={true} smooth={true} offset={-50} duration={100}><button className='btn btn-dark p-1 mx-3'>Sobre</button></Link>
                </ul>
                <ul className='nav-item'>
                  <Link to="projects" spy={true} smooth={true} offset={-50} duration={100}><button className='btn btn-dark p-1 mx-3'>Projetos</button></Link>
                </ul>
                <ul className='nav-item'>
                  <Link to="contact" spy={true} smooth={true} offset={-50} duration={100}><button className='btn btn-dark p-1 mx-3'> Contato</button></Link>
                </ul>
              </li>
            </Nav>
          </Navbar.Collapse>
        </Container>
      </Navbar>
    </header>
  );
}

export default Header;