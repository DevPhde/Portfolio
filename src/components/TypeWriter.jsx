import React, { useState, useEffect } from "react";

function TypeWriter({ messages, delay }) {
  const [messageIndex, setMessageIndex] = useState(0);
  const [charIndex, setCharIndex] = useState(0);
  const [isDeleting, setIsDeleting] = useState(false);
  const [displayMessage, setDisplayMessage] = useState("");

  useEffect(() => {
    const timer = setTimeout(() => {
      const currentMessage = messages[messageIndex];
      const currentChar = currentMessage[charIndex];

      setDisplayMessage(
        isDeleting
          ? currentMessage.substring(0, charIndex - 1)
          : currentMessage.substring(0, charIndex + 1)
      );

      setCharIndex((prevCharIndex) =>
        isDeleting ? prevCharIndex - 1 : prevCharIndex + 1
      );

      if (isDeleting) {
        if (displayMessage === "") {
          setIsDeleting(false);
          setMessageIndex((prevMessageIndex) =>
            prevMessageIndex === messages.length - 1
              ? 0
              : prevMessageIndex + 1
          );
        }
      } else {
        if (displayMessage === currentMessage) {
          setIsDeleting(true);
        }
      }
    }, delay);

    return () => clearTimeout(timer);
  }, [charIndex, delay, displayMessage, isDeleting, messageIndex, messages]);

  return <span>{displayMessage}</span>;
}

export default TypeWriter;
