import Header from "../components/Header"
import About from "../components/About"
import Projects from "../components/Projects"
function Index() {

    return (
        <div>
            <Header />
            <About />
            <Projects />
        </div>
    )
}

export default Index